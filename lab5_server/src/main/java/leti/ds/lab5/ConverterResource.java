package leti.ds.lab5;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;

@Path("/amount")
public class ConverterResource {
    @Path("/{amount}*{ratio}")
    @GET
    @Produces("application/json")
    public Double getProductInJSON(@PathParam("amount") String amount, @PathParam("ratio") String ratio) {
        return Double.parseDouble(amount) * Double.parseDouble(ratio);
    }
}