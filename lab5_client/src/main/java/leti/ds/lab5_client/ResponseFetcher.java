package leti.ds.lab5_client;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "ResponseFetcher", value = "/")
public class ResponseFetcher extends HttpServlet {
    private String serverURLPath = "http://localhost:8080/lab5-1.0-SNAPSHOT/api/amount/";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        Integer amount = Integer.parseInt(request.getParameter("amount"));
        Double ratio = Double.parseDouble(request.getParameter("ratio"));

        try {
            URL serverURL = new URL(constructQuery(amount, ratio));
            out.println(getResponse(serverURL));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getResponse(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }

        BufferedReader responseBufferedReader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String out = responseBufferedReader.readLine();

        conn.disconnect();
        return out;
    }

    private String constructQuery(Integer amount, Double ratio) {
        return serverURLPath + amount.toString() + "*" + ratio.toString();
    }
}